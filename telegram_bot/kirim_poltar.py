import telebot
import random
import datetime
import locale

# set locale ke Bahasa Indonesia
locale.setlocale(locale.LC_TIME, 'id_ID.utf8')

# inisialisasi bot telegram
bot = telebot.TeleBot('6044538145:AAEGkj57b8OUeK3-I6yoFpJw5mvfZKukuZo')

# fungsi untuk mengirim pesan ke grup
def send_message():
    angka = list(range(10))
    angka.remove(0)
    angka_acak = []

    for i in range(7):
        pilihan = random.choice(angka)
        angka_acak.append(str(pilihan))
        angka.remove(pilihan)

    angka_acak_str = ''.join(angka_acak)
    shio = ["Tikus", "Kerbau", "Harimau"]
    pilihan_shio = random.sample(shio, 2)
    now = datetime.datetime.now()

    # hasil output yang akan dikirimkan
    dash = "===================="
    isinya = f'POLA TARUNG HARI INI\n{now.strftime("%A, %d %B %Y")}'
    bbfs = "BBFS: " + angka_acak_str
    ass = "AS: " + angka_acak[0] + angka_acak[1] + angka_acak[2] + angka_acak[3] + angka_acak[4]
    kop = "KOP: " + angka_acak[0] + angka_acak[1] + angka_acak[2] + angka_acak[5] + angka_acak[6]
    kepala = "KEPALA: " + angka_acak[3] + angka_acak[4] + angka_acak[5] + angka_acak[6] + angka_acak[0]
    ekor = "EKOR: " + angka_acak[3] + angka_acak[4] + angka_acak[0] + angka_acak[1] + angka_acak[2]
    shio = "shio: " + pilihan_shio[0] + " & " + pilihan_shio[1]
    link = "\nLink Daftar: https://heylink.me/jangkrik4d.com"

    # menggabungkan hasil output menjadi satu pesan
    message = '\n'.join([dash, isinya, dash, bbfs, ass, kop, kepala, ekor, shio, link])

    chat_ids = ['@bonushunter2023', '5672851004'] # 123456789 adalah ID numerik grup pribadi / tele pribadi
    for chat_id in chat_ids:
        bot.send_message(chat_id, message)

# panggil fungsi send_message untuk mengirim pesan
send_message()
