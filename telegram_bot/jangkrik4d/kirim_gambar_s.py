import subprocess
import schedule
import time
import sys
import math
from termcolor import colored

def run_cambodia():
    subprocess.run(["python", "kirim_gambar_sdy.py"])

def run_sdy():
    subprocess.run(["python", "kirim_gambar_sdy.py"])

def run_china():
    subprocess.run(["python", "kirim_gambar_sdy.py"])

# Set jadwal untuk menjalankan fungsi yang telah didefinisikan
# schedule.every().day.at("10:00").do(run_cambodia)
# schedule.every().day.at("12:00").do(run_sdy)
# schedule.every().day.at("14:00").do(run_china)

schedule.every().day.at("23:01").do(run_cambodia)
schedule.every().day.at("23:04").do(run_sdy)
schedule.every().day.at("14:00").do(run_china)

try:
    while True:
        # Cek jadwal setiap 1 detik
        schedule.run_pending()

        # Menghitung countdown untuk jadwal selanjutnya
        next_schedule = schedule.idle_seconds()
        if next_schedule is not None:
            sys.stdout.write("\r")
            sys.stdout.write("Next schedule in ")
            countdown = time.strftime("%H:%M:%S", time.gmtime(next_schedule))
            sys.stdout.write(colored(countdown, 'green'))
            sys.stdout.flush()

            # Menghitung mundur waktu
            for i in range(math.floor(next_schedule)):
                time.sleep(1)
                sys.stdout.write("\r")
                sys.stdout.write("Next schedule in ")
                countdown = time.strftime("%H:%M:%S", time.gmtime(next_schedule - i - 1))
                sys.stdout.write(colored(countdown, 'green'))
                sys.stdout.flush()

        time.sleep(1)

except KeyboardInterrupt:
    # Jika pengguna menekan Ctrl+C, keluar dari program
    print("\nExiting...")
    sys.exit(0)
