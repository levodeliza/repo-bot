import telebot

# Baca token dari file
with open('token.txt', 'r') as token_file:
    token = token_file.read().strip()

# Inisialisasi bot telegram
bot = telebot.TeleBot(token)

# Baca chat_ids dari file
with open('chat_ids.txt', 'r') as chat_ids_file:
    chat_ids = chat_ids_file.read().strip().split('\n')

# Fungsi untuk mengirim pesan ke grup dengan gambar
def send_message():
    image_path = 'img/promo/new_100.png'
    for chat_id in chat_ids:
        with open(image_path, 'rb') as photo:
            bot.send_photo(chat_id, photo, caption='Promo: NEW MEMBER BONUS 100% UNTUK SLOT\nLink Situs: https://rebrand.ly/jangkrik4d')

# panggil fungsi send_message untuk mengirim pesan
send_message()
