import subprocess
import schedule
import time
import sys
import math
from termcolor import colored

def run_promo1():
    subprocess.run(["python", "kirim_promo_bonus_happy.py"])
def run_promo2():
    subprocess.run(["python", "kirim_promo_bonus_rolling.py"])
def run_promo3():
    subprocess.run(["python", "kirim_promo_cashback_kalah.py"])
def run_promo4():
    subprocess.run(["python", "kirim_promo_cashback_togel.py"])
def run_promo5():
    subprocess.run(["python", "kirim_promo_new_20.py"])
def run_promo6():
    subprocess.run(["python", "kirim_promo_new_100.py"])


# Set jadwal untuk menjalankan fungsi yang telah didefinisikan
schedule.every().day.at("10:00").do(run_promo1)
schedule.every().day.at("12:00").do(run_promo2)
schedule.every().day.at("14:00").do(run_promo3)
schedule.every().day.at("15:00").do(run_promo4)
schedule.every().day.at("16:00").do(run_promo5)
schedule.every().day.at("19:00").do(run_promo6)

# schedule.every().day.at("14:23").do(run_promo1)
# schedule.every().day.at("14:23").do(run_promo2)
# schedule.every().day.at("14:23").do(run_promo3)
# schedule.every().day.at("14:23").do(run_promo4)
# schedule.every().day.at("14:23").do(run_promo5)
# schedule.every().day.at("14:23").do(run_promo6)

try:
    while True:
        # Cek jadwal setiap 1 detik
        schedule.run_pending()

        # Menghitung countdown untuk jadwal selanjutnya
        next_schedule = schedule.idle_seconds()
        if next_schedule is not None:
            sys.stdout.write("\r")
            sys.stdout.write("Next schedule promo in ")
            countdown = time.strftime("%H:%M:%S", time.gmtime(next_schedule))
            sys.stdout.write(colored(countdown, 'green'))
            sys.stdout.flush()

            # Menghitung mundur waktu
            for i in range(math.floor(next_schedule)):
                time.sleep(1)
                sys.stdout.write("\r")
                sys.stdout.write("Next schedule promo in ")
                countdown = time.strftime("%H:%M:%S", time.gmtime(next_schedule - i - 1))
                sys.stdout.write(colored(countdown, 'green'))
                sys.stdout.flush()

        time.sleep(1)

except KeyboardInterrupt:
    # Jika pengguna menekan Ctrl+C, keluar dari program
    print("\nExiting...")
    sys.exit(0)
